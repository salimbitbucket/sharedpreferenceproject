package com.salim.pratice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class third extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
EditText nameid,pid,eid;
nameid=findViewById(R.id.nameid);
pid=findViewById(R.id.pid);
eid=findViewById(R.id.eid);
Button sid=findViewById(R.id.sid);

sid.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        SharedPreferences preferences=getSharedPreferences("DATA",0);
        String n=nameid.getText().toString();
        String p=pid.getText().toString();
        String e=eid.getText().toString();

        SharedPreferences.Editor editor=preferences.edit();
        editor.putString("NAME",n);
        editor.putString("PASSWORD",p);
        editor.putString("EMAIL",e);
        editor.commit();
        Intent i=new Intent(third.this,MainActivity.class);
        startActivity(i);
    }
});

    }
}