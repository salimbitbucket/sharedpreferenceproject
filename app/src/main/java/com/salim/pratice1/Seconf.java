package com.salim.pratice1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Seconf extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seconf);
        Button back=findViewById(R.id.back);
        TextView tid=findViewById(R.id.tid);

        SharedPreferences preferences=getSharedPreferences("DATA",0);
        String n1= preferences.getString("NAME","");
        String p1= preferences.getString("PASSWORD","");
        String e1= preferences.getString("EMAIL","");
        tid.setText("name="+n1+ "  Password="+p1+"  Email="+e1);



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Seconf.this,MainActivity.class);
                startActivity(i);
            }
        });
    }
}